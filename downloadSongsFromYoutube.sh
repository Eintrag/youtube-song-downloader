#!/bin/bash

songListFileName=songlist.txt
fileExtension=mp3

if [ $# -eq 1 ]
  then
    songListFileName="$1"
fi

totalNumberOfSongs=$(cat "$songListFileName" | wc -l)
numberOfDownloadedSongs=0
numberOfSkippedSongs=0

while read line; do
  lineWithoutBlanks=${line//[ ]/_}
  if [ ! -f songs/$lineWithoutBlanks.$fileExtension ]; then
    echo "Downloading songs/$line.$fileExtension"
    youtube-dl -o songs/$lineWithoutBlanks."%(ext)"s --extract-audio --audio-format "$fileExtension" "ytsearch:$line"
    ((numberOfDownloadedSongs++))
  else
    echo "songs/$line.$fileExtension was already there"
    ((numberOfSkippedSongs++))
  fi
echo "Downloaded songs: $numberOfDownloadedSongs. Skipped songs: $numberOfSkippedSongs. Songs in provided list: $totalNumberOfSongs."
done < "$songListFileName"
